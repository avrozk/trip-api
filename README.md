###Install notes:

 - copy .env.dist to .env
 - edit to enter correct api url, login and pass.
 - create database
 - composer install
 - php bin/console doctrine:schema:update --force


###Api testing

 - Import Postman collection from soap.postman_collection.json

###ServicesOnDate 
####request
```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
     xmlns:int="http://localhost/InternetSaleJSON">
   <soap:Header/>
   <soap:Body>
      <int:ServicesOnDate>
    	<int:StringJSON>
    	    {
                "date_from": "2019-01-01",
                "date_to": "2019-05-01"
         	}
         	</int:StringJSON>
       </int:ServicesOnDate>

   </soap:Body>
</soap:Envelope>
```

####response
```json
{
    "Name": "Истории разводных мостов",
    "ID": "000000013",
    "NoCategory": false,
    "ComboIn": false,
    "TypeOfService": "DateTimeSeat",
    "StatusRequestBuyer": false,
    "FormingBarCode": true
}
```
###EventsOnDate 

####request
```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
xmlns:int="http://localhost/InternetSaleJSON">
   <soap:Header/>
   <soap:Body>
      <int:EventsOnDate>
    	<int:StringJSON>{
         				"Date_From": "2019-02-17"
         				}
     	</int:StringJSON>
       </int:EventsOnDate>
   </soap:Body>
</soap:Envelope>
```

####response
```json
{
    "Name": "City Sightseeing BUS 17.02.2019 16:00:00",
    "ID": "000019378",
    "EventDateTime": "2019-02-17T16:00:00",
    "VenueID": "000000019",
    "VenueName": "City Sightseeing BUS",
    "NoSeat": true,
    "ServiceGroupName": "АВТОБУСНЫЕ ЭКСКУРСИИ",
    "ServiceGroup_ID": "Т00000005",
    "PierID": "000000002",
    "PierName": "Адмиралтейская наб, 2 | Admiralty emb, 2",
    "TypeOfService": "DateTime",
    "StatusRequestBuyer": true
}

```
