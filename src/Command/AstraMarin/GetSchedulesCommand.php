<?php

namespace App\Command\AstraMarin;

use App\Service\AstraMarin\SoapService;
use App\Wsdl\AstraMarin\ServicesOnDate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetSchedulesCommand extends Command
{
    protected static $defaultName = 'astra-marin:get-schedules';

    private $soapService;

    /**
     * GetSchedulesCommand constructor.
     * @param $soapService
     */
    public function __construct(SoapService $soapService)
    {
        $this->soapService = $soapService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('get schedules of excursions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->soapService->getClient();

        $request = new ServicesOnDate(json_encode([
            'date_from' => '2018-05-01',
            'date_to' => '2019-05-01',
        ]));
        
        $result = $client->ServicesOnDate($request);

        $output->write(print_r($result,true));
        //todo save actual data

    }
}
