<?php

namespace App\Command\AstraMarin;

use App\Entity\City;
use App\Entity\Company;
use App\Entity\Country;
use App\Entity\Excursion;
use App\Entity\Port;
use App\Service\AstraMarin\SoapService;
use App\Wsdl\AstraMarin\EventsOnDate;
use App\Wsdl\AstraMarin\SeatsOnEvent;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportExcursionsCommand extends Command
{
    protected static $defaultName = 'astra-marin:import-excursions';

    private $soapService;
    private $em;

    /**
     * GetSchedulesCommand constructor.
     * @param SoapService $soapService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SoapService $soapService, EntityManagerInterface $entityManager)
    {
        $this->soapService = $soapService;
        $this->em = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Import excursions from soap API')
            ->addArgument('start', InputArgument::OPTIONAL, 'Start date in 01.01.2000 format, default is today.')
            ->addArgument('days', InputArgument::OPTIONAL, 'Days to iterate, default is 3.');
    }

    private function getItems($results, $field = 'Event')
    {
        return json_decode($results->getReturn()->$field[0]->getStringJson(), true);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->soapService->getClient();

        $days = $input->getArgument('days');

        if ($days) {
            $totalDaysIterate = $days-1;
        } else {
            $totalDaysIterate = 3;
        }

        $start = $input->getArgument('start');

        if ($start) {
            $now = DateTime::createFromFormat('d.m.Y', $start);
        } else {
            $now = new \DateTime();
        }

        $dateTo = clone $now;
        $dateTo->add(new \DateInterval('P' . $totalDaysIterate . 'D'));
        $formattedDateTo = $dateTo->format('Y-m-d');
        $formattedDateFrom = $now->format('Y-m-d');

        $output->writeln('now process data for period from : ' . $formattedDateFrom . ' to ' . $formattedDateTo);
        $request = new EventsOnDate(json_encode([
            'Date_From' => $formattedDateFrom,//'2019-03-18',
            'Date_To' => $formattedDateTo
        ]));

        $results = $this->getItems($client->EventsOnDate($request));

        $progressBar = new ProgressBar($output, count($results));
        $progressBar->start();

        foreach ($results as $key => $row) {

            if (!in_array($row['ServiceGroupName'], $this->getAllowedServiceTypes())){
                continue;
            }

            $currentDate = (new DateTime($row['EventDateTime']));
            $progressBar->advance();

            $excursionNameFromApi = preg_replace('/ \d{2}.\d{2}.\d{4} \d{1,2}:\d{2}:\d{2}/', '', $row['Name']);

            $excursion = $this->em->getRepository(Excursion::class)
                ->findOneBy([
                    'name' => $excursionNameFromApi,
                ]);

            if (!$excursion) {
                $excursion = new Excursion();
                $excursion
                    ->setName(
                        $excursionNameFromApi
                    )
                    ->setDuration(
                        $row['Duration']
                    )
                ;
            }

            //country set
            $country = $this->em->getRepository(Country::class)
                ->findOneBy([
                    'name' => 'Россия'
                ]);

            if (!$country) {
                $country = (new Country())
                    ->setName('Россия')
                    ->setIso2('RU')
                    ->setIso3('RUS')
                ;

                $this->em->persist($country);
                $this->em->flush();
            }

            //city set
            $city = $this->em->getRepository(City::class)
                ->findOneBy([
                    'name' => 'Санкт-Петербург'
                ]);

            if (!$city) {
                $city = (new City())
                    ->setName('Санкт-Петербург')
                    ->setCountry($country)
                ;
                $this->em->persist($city);
                $this->em->flush();
            }

            $company = $this->em->getRepository(Company::class)
                ->findOneBy([
                    'name' => 'ASTRA MARINE'
                ]);
            if (!$company) {
                $company = (new Company())
                    ->setName('ASTRA MARINE')
                    ->setPhone('+7 (812) 426-17-17')//todo where to get phone?
                    ->setEmail('booking@astra-marine.ru') //todo where to get email?
                ;
                $this->em->persist($company);
                $this->em->flush();
            }

            $seatOnEvent = $client->SeatsOnEvent(
                new SeatsOnEvent(json_encode([
                    'Event_ID' => $row['ID'],
                ]))
            );

            $seatsCategories = $this->getItems($seatOnEvent, 'Seats');

            $excursion->setCompany($company);

            $pierName = $this->extractPierName($row['PierName']);

            $pier = $this->em->getRepository(Port::class)
                ->findOneBy(
                    [
                        'name' => $pierName
                    ]
                );

            if (!$pier){
                $pier = (new Port())
                    ->setName($pierName)
                    ->setAddress($pierName)
                    ->setCity($city)
                ;

                $this->em->persist($pier);
            }

            $excursion->setPort($pier);

            $this->em->persist($excursion);

            foreach ($seatsCategories as $seatsCategory) {

                foreach ($seatsCategory['Prices'] as $price) {

                    if ($seatsCategory['CategorySeatName'] == '') {
                        $seatsCategory['CategorySeatName'] = 'noName';
                    }

                    $excursionSchedule = $this->findExcursionScheduleByDate(
                        $excursion,
                        $seatsCategory['CategorySeatName'] ,
                        $currentDate
                    );

                    $dayOfWeek = (int)(new \DateTime($row['EventDateTime']))->format('N');

                    $excursionTarifCategory = $this->em->getRepository(Excursion\Tarif\Category::class)
                        ->findOneBy([
                            'name' => $this->getTarifCategoryName($price['PriceName'])
                        ]);

                    if (!$excursionTarifCategory) {
                        $excursionTarifCategory = (new Excursion\Tarif\Category())
                            ->setName($this->getTarifCategoryName($price['PriceName']));
                    }

                    $excursionTarifSchedule = $this->em->getRepository(Excursion\Tarif\Schedule::class)
                        ->findOneBy([
                            'tarif' => $excursionSchedule->getTarif(),
                            'category' => $excursionTarifCategory,
                            'time_from' => new \DateTime($row['EventDateTime']),

                        ]);

                    if (!$excursionTarifSchedule) {
                        $excursionTarifSchedule = (new Excursion\Tarif\Schedule())
                            ->setTarif($excursionSchedule->getTarif())
                            ->setTimeFrom(new \DateTime($row['EventDateTime']))
                            ->setCategory($excursionTarifCategory)
                            ->setTimeTo(
                                $this->getTimeToFromDuration($row['EventDateTime'], $excursion->getDuration())
                            );
                    };

                    $excursionTarifSchedule
                        ->{'setDay' . $dayOfWeek}($price['Price']);

                    $excursionTarifSchedule = $this->fillEmptyDays($excursionTarifSchedule);
                    #проставляем пустую цену для других дней

                    $this->em->persist($excursionTarifCategory);
                    $this->em->persist($excursionSchedule->getTarif());
                    $this->em->persist($excursionSchedule);
                    $this->em->persist($excursionTarifSchedule);
                    $this->em->flush();


                }
            }
        }

        $progressBar->finish();

        $this->em->flush();
    }

    private function fillEmptyDays($excursionTarifSchedule)
    {
        #проставляем пустую цену для других дней
        #todo убрать, если не нужно

        for ($i = 1; $i <= 7; $i++) {
            if (!$excursionTarifSchedule->{'getDay' . $i}()) {
                $excursionTarifSchedule->{'setDay' . $i}(0);
            }
        }

        return $excursionTarifSchedule;
    }

    private function getTarifCategoryName($rawName) {
        $nameArr = explode(' | ', $rawName);

        if (count($nameArr) !== 2) {

            $nameArr = explode(' ', $rawName);

            if (count($nameArr) !== 2) {
                throw new \Exception('can\'t parse tarif category name:' . $rawName);
            }
        }

        return $nameArr[0];
    }

    private function getTimeToFromDuration($starTime, $duration)
    {
        $start = new \DateTime($starTime);
        if ($duration === 0) {
            $finalTime = $start;
        } elseif ($duration > 0) {
            $finalTime = $start
                ->add(new \DateInterval('PT'.$duration.'M'))
            ;
        } else {
            throw new \Exception('Wrong duration value in excursion');
        }

        return $finalTime;
    }

    private function getAllowedServiceTypes()
    {
        return [
            'ВОДНЫЕ КРУИЗЫ',
            'МЕТЕОРЫ В ПЕТЕРГОФ'
        ];
    }

    private function extractPierName($pierName)
    {
        $pierArray = explode(' | ', $pierName);

        if (count($pierArray) !== 2) {
            throw new \Exception('can\'t extract pier from ' . $pierName);
        }

        return $pierArray[0];
    }

    private function findExcursionScheduleByDate(Excursion $excursion, $tarifName, DateTime $currentDate)
    {
        $currentDate->setTime(0,0,0);

        $dateFrom = clone $currentDate;
        $dateTo = clone $currentDate;

        $dayOfWeek = $currentDate->format('N');
        $dateFrom->sub(new \DateInterval('P'.($dayOfWeek-1).'D'));
        $dateTo->add(new \DateInterval('P'.(7-$dayOfWeek).'D'));

        $excursionSchedule = $this->em->getRepository(Excursion\Schedule\Schedule::class)
            ->findByTarifName(
                $excursion,
                $tarifName,
                $dateFrom,
                $dateTo
            );

        $excursionTarif = (new Excursion\Tarif\Tarif())
                            ->setName($tarifName)
                            ->setColor('#000000');

        if (!$excursionSchedule) {
            $excursionSchedule = (new Excursion\Schedule\Schedule())
                ->setExcursion($excursion)
                ->setTarif($excursionTarif)
                ->setDateFrom($dateFrom)
                ->setDateTo($dateTo)
            ;
        }

        return $excursionSchedule;
    }
}
