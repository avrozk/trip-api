<?php

namespace App\Service\AstraMarin;


use App\Wsdl\AstraMarin\InternetSaleJSON;

class SoapService {
    private $url;
    private $login;
    private $password;
    private $proxy;

    /**
     * SoapService constructor.
     * @param string $apiUrl
     * @param string $apiLogin
     * @param string $apiPassword
     * @param null|string $proxy
     */
    public function __construct(string $apiUrl, string $apiLogin, string $apiPassword, ?string $proxy)
    {
        $this->url = $apiUrl;
        $this->login = $apiLogin;
        $this->password = $apiPassword;
        $this->proxy = $proxy;

    }

    /**
     * @return InternetSaleJSON
     */
    public function getClient(){

        $proxy_params = [];
        if ($this->proxy) {
            $proxy_params = [
                'proxy_host' => explode(':',$this->proxy)[0],
                'proxy_port' => explode(':',$this->proxy)[1],
            ];
        }

        $client = new InternetSaleJSON(
            array_merge(
                [
                    'login' => $this->login,
                    'password' => $this->password,
                    'trace' => 1,
                    'soap_version' => SOAP_1_2,
                ],
                $proxy_params
            )
                ,
            "$this->url" . "?wsdl"
        );

        //need to set location because of wrong URI action in WSDL file.
        $client->__setLocation($this->url);

        return $client;
    }
}