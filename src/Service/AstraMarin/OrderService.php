<?php

namespace App\Service\AstraMarin;

use App\Wsdl\AstraMarin\BookingSeat;
use App\Wsdl\AstraMarin\CancelBookingSeat;
use App\Wsdl\AstraMarin\ChekDeposit;
use App\Wsdl\AstraMarin\EventsOnDate;
use App\Wsdl\AstraMarin\GetSeatPrice;
use App\Wsdl\AstraMarin\GetTicketType;
use App\Wsdl\AstraMarin\InternetSaleJSON;
use App\Wsdl\AstraMarin\PaymentConfirmation;
use App\Wsdl\AstraMarin\SaleSeat;
use App\Wsdl\AstraMarin\Seatategory;
use App\Wsdl\AstraMarin\ServicesOnDate;

class OrderService {
    private $soapService;

    public function __construct(SoapService $soapService)
    {
        $this->soapService = $soapService;
    }

    private function getServiceId($datetime, $client, $excursionName) {

        $request = json_encode([
            'date_from' => $datetime,
            'date_to' => $datetime,
        ]);

        $servicesOnDateResponse = $client->ServicesOnDate(new ServicesOnDate($request));
        $servicesOnDate = json_decode($servicesOnDateResponse->getReturn()->Services[0]->getStringJSON(),true);

        $serviceIds = array_filter($servicesOnDate, function ($el) use ($excursionName){
            if (isset($el['Name']) && isset($el['ID']) && $el['Name'] === $excursionName) {
                return true;
            } else {
                return false;
            }
        });

        if (count($serviceIds) == 1) {
            $serviceID = array_values($serviceIds)[0]['ID'];
        }

        return $serviceID;
    }

    private function getTicketTypeId($ticketTypeName, $client, $serviceID){
        //getServices
        $request = json_encode([
            'Service_ID' => $serviceID,
        ]);

        $ticketTypesResponse = $client->GetTicketType(new GetTicketType($request));

        $ticketTypes = json_decode($ticketTypesResponse->getReturn()->TicketType[0]->getStringJSON(),true);

        $ticketTypeIds = array_filter($ticketTypes, function ($el) use ($ticketTypeName){
            if (isset($el['Name']) && isset($el['ID']) && (strpos($el['Name'],$ticketTypeName) !== false) ) {
                return true;
            } else {
                return false;
            }
        });

        if (count($ticketTypeIds) == 1) {
            $ticketTypeId = array_values($ticketTypeIds)[0]['ID'];
        } else {
            throw new \Exception('ticket type '. $ticketTypeName . ' not found');
        }

        return $ticketTypeId;
    }

    private function getEvent($excursionName, InternetSaleJSON $client, $date, $time)
    {
        $request = json_encode([
            'Date_From' => $date,
            'Date_To' => $date,
        ]);

        $eventsResponse = $client->EventsOnDate(new EventsOnDate($request));

        $events = json_decode($eventsResponse->getReturn()->Event[0]->getStringJSON(),true);

        $events = array_filter($events, function ($el) use ($time, $excursionName, $date){
            if (isset($el['Name']) && isset($el['ID'])
                && (strpos($el['EventDateTime'],$time) !== false)
                && (strpos($el['Name'],$excursionName) !== false)
                && (strpos($el['EventDateTime'],$date) !== false)
            ) {
                return true;
            } else {
                return false;
            }
        });

        if (count($events) == 1) {
            $event = array_values($events)[0];
        } else {
            throw new \Exception('event with name '. $excursionName . ' and datetime '.$date . $time.' not found');
        }

        return $event;
    }

    public function Order($sessionId, $orderId, $excursionName = 'City Sightseeing NEVA', $date = '2019-05-06', $ticketTypeName = '1 День',
                          $time = '11:00:00', $priceType= 'Полный', $seatCategoryName = 'Standard'){

        $client = $this->soapService->getClient();

        //getServices
        $serviceID = $this->getServiceId($date, $client, $excursionName);

        //GetTicketType
        $ticketTypeId = $this->getTicketTypeId($ticketTypeName, $client, $serviceID);

        $event = $this->getEvent($excursionName, $client, $date, $time);

        $eventId = $event['ID'];
        $venueId = $event['VenueID'];

        $typePrice = $this->getTypePrice($client, $eventId, $ticketTypeId, $priceType);

        $typePriceId = $typePrice['TypePriceID'];

        $categoryId = $this->getCategoryId($client, $venueId, $eventId, $seatCategoryName);

        $order = $this->saleSeat(
            $client,
            $sessionId,
            $orderId,
            $email = 'anyships@yandex.ru',
            $eventId,
            $typePriceId,
            $categoryId,
            $ticketTypeId,
            $quantity = 1
        );

        return $order;
    }

    public function create($eventId, $seatId) {
        $client = $this->soapService->getClient();

        $uuid = uniqid();

        $request = json_encode([
            'Event_ID' => $eventId,
            'Seat_ID' => $seatId,
            'Session_ID' => $uuid,
        ]);

        $response = $client->BookingSeat(new BookingSeat($request));

        return $response;
    }

    public function confirm($orderId, $confirmation = true) {
        $client = $this->soapService->getClient();

        $request = json_encode([
            "Order_ID" => $orderId,
            "Confirmation" => $confirmation,
        ]);

        $response = $client->PaymentConfirmation(new PaymentConfirmation($request));

        return $response;

    }

    public function cancel($eventId, $sessionId) {
        $client = $this->soapService->getClient();

        $request = json_encode([
            'Event_ID' => $eventId,
            'Session_ID' => $sessionId,
        ]);

        $response = $client->CancelBookingSeat(new CancelBookingSeat($request));

        return $response;
    }

    public function checkDeposit($email, $amountOrder) {
        $client = $this->soapService->getClient();

        $request = json_encode([
            "Email" => $email,
            "AmountOrder" => $amountOrder,
        ]);

        $response = $client->ChekDeposit(new ChekDeposit($request));

        return $response;
    }

    private function getTypePrice(InternetSaleJSON $client, $eventId, $ticketTypeId, $typePriceName = 'Полный')
    {
        $request = json_encode([
            'Event_ID' => $eventId,
            'TicketType_ID' => $ticketTypeId,
        ]);

        $typePriceResponse = $client->GetSeatPrice(new GetSeatPrice($request));
        $typePrices = json_decode($typePriceResponse->getReturn()->Price[0]->getStringJSON(),true);

        $prices = array_filter($typePrices, function ($el) use ($typePriceName){
            if (isset($el['TypePriceName']) && isset($el['TypePriceID'])
                && (strpos($el['TypePriceName'], $typePriceName) !== false)
            ) {
                return true;
            } else {
                return false;
            }
        });

        return array_values($prices)[0];
    }

    private function getCategoryId(InternetSaleJSON $client, $venueId, $eventId, $categoryName = 'Standard')
    {
        $request = json_encode([
            'Venue_ID' => $venueId,
            'Event_ID' => $eventId,
        ]);

        $seatCategoriesResponse = $client->Seatategory(new Seatategory($request));

        $seatCategories = json_decode($seatCategoriesResponse->getReturn()->SeatsСategory[0]->getStringJSON(),true);

        $seatCategoriesFiltered = array_filter($seatCategories, function ($el) use ($categoryName){
            if (isset($el['CategoryName']) && isset($el['CategoryID'])
                && (strpos($el['CategoryName'], $categoryName) !== false)
            ) {
                return true;
            } else {
                return false;
            }
        });

        if (count($seatCategoriesFiltered) == 1) {
            $seatCategoryId = array_values($seatCategoriesFiltered)[0]['CategoryID'];
        } else {
            throw new \Exception('Seat category with name '. $categoryName . ' not found');
        }

        return $seatCategoryId;
    }

    private function saleSeat(InternetSaleJSON $client, $sessionId, $orderId, $email, $eventId, $typePriceId,
                              $categoryId, $ticketTypeId, $quantity)
    {
        $request = json_encode([
            "Session_ID" =>  $sessionId,
            "Order_ID" =>  $orderId,
            "Email" =>  $email,
            "Order" =>  [
                "Event_ID" =>  $eventId,
                "TypePrice_ID" =>  $typePriceId, //получаем из   GetSeatPrice(Event_id, TicketType_id);
                "Category_ID" =>  $categoryId, // получаем из SeatCategory(Venue_ID, Event_ID)
                "TicketType_ID" =>  $ticketTypeId, //получаем из GetTicketType (Service_ID), Service_ID
                "QuantityOfTickets" =>  $quantity
            ]
        ]);

        //todo получить тело ответа, на д.м. приходит ошибка от soap api
        $saleSeatResponse = $client->SaleSeat(new SaleSeat($request));

        return $saleSeatResponse;
    }
}