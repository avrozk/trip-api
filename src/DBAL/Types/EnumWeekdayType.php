<?php

namespace App\DBAL\Types;


class EnumWeekdayType extends EnumType
{
    protected static $name = 'enum_weekday';

    const day1 = 'Понедельник';
    const day2 = 'Вторник';
    const day3 = 'Среда';
    const day4 = 'Четверг';
    const day5 = 'Пятница';
    const day6 = 'Суббота';
    const day7 = 'Воскресенье';

    protected static $values = [
        self::day1,
        self::day2,
        self::day3,
        self::day4,
        self::day5,
        self::day6,
        self::day7,
    ];
}