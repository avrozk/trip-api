<?php

namespace App\DBAL\Types;


class EnumDurationType extends EnumType
{
    protected static $name = 'enumdurationtype';
    const A = 'Journee' ;
    const B = 'Weekend' ;
    const C = '3jours' ;
    const D = '5jours' ;
    const E = '7jours' ;
    protected static $values = [
        self::A,
        self::B,
        self::C,
        self::D,
        self::E
    ];
}