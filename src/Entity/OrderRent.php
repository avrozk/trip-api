<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRentRepository")
 */
class OrderRent extends BaseOrder
{
    public function getType(){
        return self::TYPE_RENT;
    }

    public function getTypeName(){
        return 'Аренда';
    }
}
