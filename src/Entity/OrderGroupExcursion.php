<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderGroupExcursionRepository")
 */
class OrderGroupExcursion extends BaseOrder
{
    public function getType(){
        return self::TYPE_GROUP_EXCURSION;
    }

    public function getTypeName(){
        return 'Групповая экскурсия';
    }
}
