<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseOrderRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="smallint")
 * @ORM\DiscriminatorMap({
 *     1 = "OrderRent",
 *     2 = "OrderGroupExcursion"
 * })
 * @ORM\Table(name="order_list", indexes={@ORM\Index(name="type_idx", columns={"type"})})
 */
abstract class BaseOrder extends BaseOrderRequest
{
    const TYPE_RENT = 1;
    const TYPE_GROUP_EXCURSION  = 2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ship", inversedBy="orderRents")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $ship;

    public function getShip(): ?Ship
    {
        return $this->ship;
    }

    public function setShip(?Ship $ship): self
    {
        $this->ship = $ship;

        return $this;
    }

    abstract public function getType();

    abstract public function getTypeName();
}
