<?php

namespace App\Entity;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Excursion\Schedule\Schedule;
use App\Entity\Excursion\Schedule\ScheduleExclude;
use App\Entity\Excursion\VoyageExclude\VoyageExclude;
use App\Entity\Excursion\Voyage\Voyage;
use App\Entity\Port;
use App\Entity\Ship;
use Beelab\TagBundle\Entity\AbstractTaggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExcursionRepository")
 * @ORM\Table(name="excursion")
 */
class Excursion extends AbstractTaggable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Port")
     */
    private $port;

    /**
     * Продолжительность экскурсии в минутах
     *
     * @ORM\Column(type="integer")
     */
    private $duration = 0;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(name="excursions_tags",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    protected $tags;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="integer")
     */
    private $basePrice = 0;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ship", cascade={"persist"})
     * @ORM\JoinTable(name="excursion_available_ships",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ship_id", referencedColumnName="id")}
     *      )
     */
    private $availableShips;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Excursion\Schedule\Schedule", mappedBy="excursion")
     */
    private $schedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Excursion\Schedule\ScheduleExclude", mappedBy="excursion", orphanRemoval=true)
     */
    private $scheduleExcludes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Excursion\Voyage\Voyage", mappedBy="excursion", orphanRemoval=true)
     */
    private $voyages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Excursion\VoyageExclude\VoyageExclude", mappedBy="excursion", orphanRemoval=true)
     */
    private $voyageExcludes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    public function __construct()
    {
        parent::__construct();
        $this->availableShips = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->scheduleExcludes = new ArrayCollection();
        $this->voyages = new ArrayCollection();
        $this->voyageExcludes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $tagsText
     * @return void
     * @throws \Exception
     */
    public function setTagsText(?string $tagsText): void
    {
        $this->updated = new \DateTimeImmutable();
        parent::setTagsText($tagsText);

    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getBasePrice(): ?int
    {
        return $this->basePrice;
    }

    public function setBasePrice(int $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPort(): ?Port
    {
        return $this->port;
    }

    public function setPort(?Port $port): self
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return Collection|Ship[]
     */
    public function getAvailableShips(): Collection
    {
        return $this->availableShips;
    }

    public function addAvailableShip(Ship $availableShip): self
    {
        if (!$this->availableShips->contains($availableShip)) {
            $this->availableShips[] = $availableShip;
        }

        return $this;
    }

    public function removeAvailableShip(Ship $availableShip): self
    {
        if ($this->availableShips->contains($availableShip)) {
            $this->availableShips->removeElement($availableShip);
        }

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->setExcursion($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->contains($schedule)) {
            $this->schedules->removeElement($schedule);
            // set the owning side to null (unless already changed)
            if ($schedule->getExcursion() === $this) {
                $schedule->setExcursion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ScheduleExclude[]
     */
    public function getScheduleExcludes(): Collection
    {
        return $this->scheduleExcludes;
    }

    public function addScheduleExclude(ScheduleExclude $scheduleExclude): self
    {
        if (!$this->scheduleExcludes->contains($scheduleExclude)) {
            $this->scheduleExcludes[] = $scheduleExclude;
            $scheduleExclude->setExcursion($this);
        }

        return $this;
    }

    public function removeScheduleExclude(ScheduleExclude $scheduleExclude): self
    {
        if ($this->scheduleExcludes->contains($scheduleExclude)) {
            $this->scheduleExcludes->removeElement($scheduleExclude);
            // set the owning side to null (unless already changed)
            if ($scheduleExclude->getExcursion() === $this) {
                $scheduleExclude->setExcursion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Voyage[]
     */
    public function getVoyages(): Collection
    {
        return $this->voyages;
    }

    public function addVoyage(Voyage $voyage): self
    {
        if (!$this->voyages->contains($voyage)) {
            $this->voyages[] = $voyage;
            $voyage->setExcursion($this);
        }

        return $this;
    }

    public function removeVoyage(Voyage $voyage): self
    {
        if ($this->voyages->contains($voyage)) {
            $this->voyages->removeElement($voyage);
            // set the owning side to null (unless already changed)
            if ($voyage->getExcursion() === $this) {
                $voyage->setExcursion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VoyageExclude[]
     */
    public function getVoyageExcludes(): Collection
    {
        return $this->voyageExcludes;
    }

    public function addVoyageExclude(VoyageExclude $voyageExclude): self
    {
        if (!$this->voyageExcludes->contains($voyageExclude)) {
            $this->voyageExcludes[] = $voyageExclude;
            $voyageExclude->setExcursion($this);
        }

        return $this;
    }

    public function removeVoyageExclude(VoyageExclude $voyageExclude): self
    {
        if ($this->voyageExcludes->contains($voyageExclude)) {
            $this->voyageExcludes->removeElement($voyageExclude);
            // set the owning side to null (unless already changed)
            if ($voyageExclude->getExcursion() === $this) {
                $voyageExclude->setExcursion(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
