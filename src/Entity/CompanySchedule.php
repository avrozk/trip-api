<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyScheduleRepository")
 */
class CompanySchedule
{
    const WEEK_DAYS = [
        'Пн' => 1,
        'Вт' => 2,
        'Ср' => 3,
        'Чт' => 4,
        'Пт' => 5,
        'Сб' => 6,
        'Вс' => 7,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     * @Groups({"default"})
     * @Type("DateTime<'H:i'>")
     */
    private $time_from;

    /**
     * @ORM\Column(type="time")
     * @Groups({"default"})
     * @Type("DateTime<'H:i'>")
     */
    private $time_to;

    /**
     * @ORM\Column(type="simple_array")
     * @Assert\NotBlank()
     * @Groups({"default"})
     */
    private $week_days = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="companySchedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->time_to;
    }

    public function setTimeTo(\DateTimeInterface $time_to): self
    {
        $this->time_to = $time_to;

        return $this;
    }

    public function getWeekDays(): ?array
    {
        return $this->week_days;
    }

    public function setWeekDays(array $week_days): self
    {
        $this->week_days = $week_days;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
