<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRequestRepository")
 */
class OrderRequest extends BaseOrderRequest
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShipType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ship_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShipType(): ?ShipType
    {
        return $this->ship_type;
    }

    public function setShipType(?ShipType $ship_type): self
    {
        $this->ship_type = $ship_type;

        return $this;
    }
}
