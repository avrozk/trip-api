<?php

namespace App\Entity\Excursion\Voyage;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Excursion\Voyage\Schedule\ScheduleRepository")
 * @ORM\Table(name="excursion_voyage_schedule")
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     */
    private $time_from;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion\Voyage\Voyage", inversedBy="schedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voyage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_1 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_2 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_3 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_4 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_5 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_6 = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $day_7 = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getVoyage(): ?Voyage
    {
        return $this->voyage;
    }

    public function setVoyage(?Voyage $voyage): self
    {
        $this->voyage = $voyage;

        return $this;
    }

    public function getDay1(): ?bool
    {
        return $this->day_1;
    }

    public function setDay1(bool $day_1): self
    {
        $this->day_1 = $day_1;

        return $this;
    }

    public function getDay2(): ?bool
    {
        return $this->day_2;
    }

    public function setDay2(bool $day_2): self
    {
        $this->day_2 = $day_2;

        return $this;
    }

    public function getDay3(): ?bool
    {
        return $this->day_3;
    }

    public function setDay3(bool $day_3): self
    {
        $this->day_3 = $day_3;

        return $this;
    }

    public function getDay4(): ?bool
    {
        return $this->day_4;
    }

    public function setDay4(bool $day_4): self
    {
        $this->day_4 = $day_4;

        return $this;
    }

    public function getDay5(): ?bool
    {
        return $this->day_5;
    }

    public function setDay5(bool $day_5): self
    {
        $this->day_5 = $day_5;

        return $this;
    }

    public function getDay6(): ?bool
    {
        return $this->day_6;
    }

    public function setDay6(bool $day_6): self
    {
        $this->day_6 = $day_6;

        return $this;
    }

    public function getDay7(): ?bool
    {
        return $this->day_7;
    }

    public function setDay7(bool $day_7): self
    {
        $this->day_7 = $day_7;

        return $this;
    }
}
