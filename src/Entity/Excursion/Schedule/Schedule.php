<?php

namespace App\Entity\Excursion\Schedule;

use App\Entity\Excursion;
use App\Entity\Excursion\Tarif\Tarif;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Excursion\Schedule\ScheduleRepository")
 * @ORM\Table(name="excursion_schedule")
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_from;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_to;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion", inversedBy="schedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $excursion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion\Tarif\Tarif")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateFrom(): ?\DateTimeInterface
    {
        return $this->date_from;
    }

    public function setDateFrom(\DateTimeInterface $date_from): self
    {
        $this->date_from = $date_from;

        return $this;
    }

    public function getDateTo(): ?\DateTimeInterface
    {
        return $this->date_to;
    }

    public function setDateTo(\DateTimeInterface $date_to): self
    {
        $this->date_to = $date_to;

        return $this;
    }

    public function getExcursion(): ?Excursion
    {
        return $this->excursion;
    }

    public function setExcursion(?Excursion $excursion): self
    {
        $this->excursion = $excursion;

        return $this;
    }

    public function getTarif(): ?Tarif
    {
        return $this->tarif;
    }

    public function setTarif(?Tarif $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }
}
