<?php

namespace App\Entity\Excursion\Schedule;

use App\Entity\Excursion;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Excursion\Schedule\ScheduleExcludeRepository")
 * @ORM\Table(name="excursion_schedule_exclude")
 */
class ScheduleExclude
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_time_from;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_time_to;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion", inversedBy="scheduleExcludes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $excursion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateTimeFrom(): ?\DateTimeInterface
    {
        return $this->date_time_from;
    }

    public function setDateTimeFrom(\DateTimeInterface $date_time_from): self
    {
        $this->date_time_from = $date_time_from;

        return $this;
    }

    public function getDateTimeTo(): ?\DateTimeInterface
    {
        return $this->date_time_to;
    }

    public function setDateTimeTo(\DateTimeInterface $date_time_to): self
    {
        $this->date_time_to = $date_time_to;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getExcursion(): ?Excursion
    {
        return $this->excursion;
    }

    public function setExcursion(?Excursion $excursion): self
    {
        $this->excursion = $excursion;

        return $this;
    }
}
