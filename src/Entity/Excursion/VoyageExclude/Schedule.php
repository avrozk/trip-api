<?php

namespace App\Entity\Excursion\VoyageExclude;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Excursion\VoyageExclude\ScheduleRepository")
 * @ORM\Table(name="excursion_voyage_exclude_schedule")
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     */
    private $time_from;

    /**
     * @ORM\Column(type="time")
     */
    private $time_to;

    /**
     * @ORM\Column(type="boolean")
     */
    private $avaliable = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion\VoyageExclude\VoyageExclude", inversedBy="schedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voyageExclude;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->time_to;
    }

    public function setTimeTo(\DateTimeInterface $time_to): self
    {
        $this->time_to = $time_to;

        return $this;
    }

    public function getVoyageExclude(): ?VoyageExclude
    {
        return $this->voyageExclude;
    }

    public function setVoyageExclude(?VoyageExclude $voyageExclude): self
    {
        $this->voyageExclude = $voyageExclude;

        return $this;
    }

    public function getAvaliable(): ?bool
    {
        return $this->avaliable;
    }

    public function setAvaliable(bool $avaliable): self
    {
        $this->avaliable = $avaliable;

        return $this;
    }
}
