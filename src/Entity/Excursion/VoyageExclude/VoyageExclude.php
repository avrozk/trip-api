<?php

namespace App\Entity\Excursion\VoyageExclude;

use App\Entity\Excursion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Excursion\VoyageExclude\VoyageExcludeRepository")
 * @ORM\Table(name="excursion_voyage_exclude")
 */
class VoyageExclude
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Excursion", inversedBy="voyageExcludes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $excursion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_from;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Excursion\VoyageExclude\Schedule", mappedBy="voyageExclude", orphanRemoval=true, cascade={"persist"})
     */
    private $schedules;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExcursion(): ?Excursion
    {
        return $this->excursion;
    }

    public function setExcursion(?Excursion $excursion): self
    {
        $this->excursion = $excursion;

        return $this;
    }

    public function getDateFrom(): ?\DateTimeInterface
    {
        return $this->date_from;
    }

    public function setDateFrom(\DateTimeInterface $date_from): self
    {
        $this->date_from = $date_from;

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->setVoyageExclude($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->contains($schedule)) {
            $this->schedules->removeElement($schedule);
            // set the owning side to null (unless already changed)
            if ($schedule->getVoyageExclude() === $this) {
                $schedule->setVoyageExclude(null);
            }
        }

        return $this;
    }
}
