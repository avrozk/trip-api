<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class BaseOrderRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_time_from;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_time_to;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Port")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $port_from;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Port")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $port_to;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $owner;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $countPeople = 0;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $countChild = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateTimeFrom(): ?\DateTimeInterface
    {
        return $this->date_time_from;
    }

    public function setDateTimeFrom(\DateTimeInterface $date_time_from): self
    {
        $this->date_time_from = $date_time_from;

        return $this;
    }

    public function getDateTimeTo(): ?\DateTimeInterface
    {
        return $this->date_time_to;
    }

    public function setDateTimeTo(\DateTimeInterface $date_time_to): self
    {
        $this->date_time_to = $date_time_to;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPortFrom(): ?Port
    {
        return $this->port_from;
    }

    public function setPortFrom(?Port $port_from): self
    {
        $this->port_from = $port_from;

        return $this;
    }

    public function getPortTo(): ?Port
    {
        return $this->port_to;
    }

    public function setPortTo(?Port $port_to): self
    {
        $this->port_to = $port_to;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCountPeople(): ?int
    {
        return $this->countPeople;
    }

    public function setCountPeople(int $countPeople): self
    {
        $this->countPeople = $countPeople;

        return $this;
    }

    public function getCountChild(): ?int
    {
        return $this->countChild;
    }

    public function setCountChild(int $countChild): self
    {
        $this->countChild = $countChild;

        return $this;
    }

}
