<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentOptionRepository")
 */
class RentOption
{
    const OPTIONAL_LIST = [
        1 => 'Обязательно',
        2 => 'По факту',
        3 => 'На борту',
        4 => 'Дополнительно',
    ];

    const UNIT_LIST = [
        1 => 'литр',
        2 => 'сутки',
        3 => 'чел.',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RentOptionCategory")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $optional;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="smallint")
     */
    private $unit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ship", inversedBy="rentOptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ship;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?RentOptionCategory
    {
        return $this->category;
    }

    public function setCategory(?RentOptionCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOptional(): ?int
    {
        return $this->optional;
    }

    public function getOptionalText(): string
    {
        return self::OPTIONAL_LIST[$this->getOptional()] ?? '';
    }

    public function setOptional(int $optional): self
    {
        $this->optional = $optional;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getUnit(): ?int
    {
        return $this->unit;
    }

    public function getUnitText(): string
    {
        return self::UNIT_LIST[$this->getUnit()] ?? '';
    }

    public function setUnit(int $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getShip(): ?Ship
    {
        return $this->ship;
    }

    public function setShip(?Ship $ship): self
    {
        $this->ship = $ship;

        return $this;
    }
}
