<?php

namespace App\Entity\Ship\Tarif;

use App\Entity\Ship\Tarif;
use App\Entity\Ship\Tarif\Schedule\Exclude;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Ship\Tarif\ScheduleRepository")
 * @ORM\Table(name="ship_tarif_schedule")
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ship\Tarif", inversedBy="schedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarif;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $time_from;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $time_to;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $price = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $min_duration;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ship\Tarif\Schedule\Exclude", mappedBy="schedule", orphanRemoval=true, cascade={"persist"})
     */
    private $excludes;

    /**
     * @ORM\Column(type="enum_weekday", nullable=true)
     */
    private $start_week_day = null;

    public function __construct()
    {
        $this->excludes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTarif(): ?Tarif
    {
        return $this->tarif;
    }

    public function setTarif(?Tarif $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->time_to;
    }

    public function setTimeTo(\DateTimeInterface $time_to): self
    {
        $this->time_to = $time_to;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getMinDuration(): ?int
    {
        return $this->min_duration;
    }

    public function setMinDuration(int $min_duration): self
    {
        $this->min_duration = $min_duration;

        return $this;
    }

    /**
     * @return Collection|Exclude[]
     */
    public function getExcludes(): Collection
    {
        return $this->excludes;
    }

    public function addExclude(Exclude $exclude): self
    {
        if (!$this->excludes->contains($exclude)) {
            $this->excludes[] = $exclude;
            $exclude->setSchedule($this);
        }

        return $this;
    }

    public function removeExclude(Exclude $exclude): self
    {
        if ($this->excludes->contains($exclude)) {
            $this->excludes->removeElement($exclude);
            // set the owning side to null (unless already changed)
            if ($exclude->getSchedule() === $this) {
                $exclude->setSchedule(null);
            }
        }

        return $this;
    }

    public function getStartWeekDay()
    {
        return $this->start_week_day;
    }

    public function setStartWeekDay($start_week_day): self
    {
        $this->start_week_day = $start_week_day;

        return $this;
    }
}
