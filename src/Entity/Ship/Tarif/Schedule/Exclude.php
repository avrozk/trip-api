<?php

namespace App\Entity\Ship\Tarif\Schedule;

use App\Entity\Ship\Tarif\Schedule;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Ship\Tarif\Schedule\ExcludeRepository")
 * @ORM\Table(name="ship_tarif_schedule_exclude")
 * @UniqueEntity(fields={"schedule", "weekDay"}, message="Exclude is already exists")
 * @UniqueEntity(fields={"schedule", "dateTimeFrom", "dateTimeTo"}, message="Exclude is already exists")
 */
class Exclude
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(groups = {"week_type"})
     */
    private $dateTimeFrom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(groups = {"week_type"})
     */
    private $dateTimeTo;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Assert\NotBlank(groups = {"day_type"})
     */
    private $weekDay;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ship\Tarif\Schedule", inversedBy="excludes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $schedule;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateTimeFrom(): ?\DateTimeInterface
    {
        return $this->dateTimeFrom;
    }

    public function setDateTimeFrom(?\DateTimeInterface $dateTimeFrom): self
    {
        $this->dateTimeFrom = $dateTimeFrom;

        return $this;
    }

    public function getDateTimeTo(): ?\DateTimeInterface
    {
        return $this->dateTimeTo;
    }

    public function setDateTimeTo(?\DateTimeInterface $dateTimeTo): self
    {
        $this->dateTimeTo = $dateTimeTo;

        return $this;
    }

    public function getWeekDay(): ?int
    {
        return $this->weekDay;
    }

    public function setWeekDay(?int $weekDay): self
    {
        $this->weekDay = $weekDay;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }
}
