<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @UniqueEntity(fields={"phone"})
 * @UniqueEntity(fields={"email"})
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Groups({"default"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"default"})
     */
    private $description = '';

    /**
     * @ORM\Column(type="string")
     * @Groups({"default"})
     */
    private $address = '';

    /**
     * @ORM\Column(type="string")
     * @Groups({"default"})
     */
    private $url = '';

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     * @Assert\NotBlank()
     * @Groups({"default"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Groups({"default"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Groups({"default"})
     */
    private $contactName = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompanySchedule", mappedBy="company", orphanRemoval=true, cascade={"persist"})
     * @Assert\Valid()
     * @Groups({"default"})
     */
    private $companySchedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ship", mappedBy="company")
     */
    private $ships;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->companySchedules = new ArrayCollection();
        $this->ships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getCompany() === $this) {
                $user->setCompany(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection|CompanySchedule[]
     */
    public function getCompanySchedules(): Collection
    {
        return $this->companySchedules;
    }

    public function addCompanySchedule(CompanySchedule $companySchedule): self
    {
        if (!$this->companySchedules->contains($companySchedule)) {
            $this->companySchedules[] = $companySchedule;
            $companySchedule->setCompany($this);
        }

        return $this;
    }

    public function removeCompanySchedule(CompanySchedule $companySchedule): self
    {
        if ($this->companySchedules->contains($companySchedule)) {
            $this->companySchedules->removeElement($companySchedule);
            // set the owning side to null (unless already changed)
            if ($companySchedule->getCompany() === $this) {
                $companySchedule->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ship[]
     */
    public function getShips(): Collection
    {
        return $this->ships;
    }

    public function addShip(Ship $ship): self
    {
        if (!$this->ships->contains($ship)) {
            $this->ships[] = $ship;
            $ship->setCompany($this);
        }

        return $this;
    }

    public function removeShip(Ship $ship): self
    {
        if ($this->ships->contains($ship)) {
            $this->ships->removeElement($ship);
            // set the owning side to null (unless already changed)
            if ($ship->getCompany() === $this) {
                $ship->setCompany(null);
            }
        }

        return $this;
    }
}
