<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentTarifRepository")
 */
class RentTarif
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $color = '#ffa500';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RentTarifSchedule", mappedBy="tarif", orphanRemoval=true, cascade={"persist"})
     */
    private $rentTarifSchedules;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    public function __construct()
    {
        $this->rentTarifSchedules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|RentTarifSchedule[]
     */
    public function getRentTarifSchedules(): Collection
    {
        return $this->rentTarifSchedules;
    }

    public function addRentTarifSchedule(RentTarifSchedule $rentTarifSchedule): self
    {
        if (!$this->rentTarifSchedules->contains($rentTarifSchedule)) {
            $this->rentTarifSchedules[] = $rentTarifSchedule;
            $rentTarifSchedule->setTarif($this);
        }

        return $this;
    }

    public function removeRentTarifSchedule(RentTarifSchedule $rentTarifSchedule): self
    {
        if ($this->rentTarifSchedules->contains($rentTarifSchedule)) {
            $this->rentTarifSchedules->removeElement($rentTarifSchedule);
            // set the owning side to null (unless already changed)
            if ($rentTarifSchedule->getTarif() === $this) {
                $rentTarifSchedule->setTarif(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
