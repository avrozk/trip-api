<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentTarifScheduleRepository")
 */
class RentTarifSchedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RentTarif", inversedBy="rentTarifSchedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarif;

    /**
     * @ORM\Column(type="time")
     */
    private $time_from;

    /**
     * @ORM\Column(type="time")
     */
    private $time_to;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_1 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_2 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_3 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_4 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_5 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_6 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $day_7 = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->time_to;
    }

    public function setTimeTo(\DateTimeInterface $time_to): self
    {
        $this->time_to = $time_to;

        return $this;
    }

    public function getDay1(): ?int
    {
        return $this->day_1;
    }

    public function setDay1(int $day_1): self
    {
        $this->day_1 = $day_1;

        return $this;
    }

    public function getDay2(): ?int
    {
        return $this->day_2;
    }

    public function setDay2(int $day_2): self
    {
        $this->day_2 = $day_2;

        return $this;
    }

    public function getDay3(): ?int
    {
        return $this->day_3;
    }

    public function setDay3(int $day_3): self
    {
        $this->day_3 = $day_3;

        return $this;
    }

    public function getDay4(): ?int
    {
        return $this->day_4;
    }

    public function setDay4(int $day_4): self
    {
        $this->day_4 = $day_4;

        return $this;
    }

    public function getDay5(): ?int
    {
        return $this->day_5;
    }

    public function setDay5(int $day_5): self
    {
        $this->day_5 = $day_5;

        return $this;
    }

    public function getDay6(): ?int
    {
        return $this->day_6;
    }

    public function setDay6(int $day_6): self
    {
        $this->day_6 = $day_6;

        return $this;
    }

    public function getDay7(): ?int
    {
        return $this->day_7;
    }

    public function setDay7(int $day_7): self
    {
        $this->day_7 = $day_7;

        return $this;
    }

    public function getTarif(): ?RentTarif
    {
        return $this->tarif;
    }

    public function setTarif(?RentTarif $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }
}
