<?php

namespace App\Entity;

use App\DBAL\Types\EnumDurationType;
use App\Entity\Ship\Tarif;
use App\Model\Currency;
use App\Model\Lang;
use Beelab\TagBundle\Entity\AbstractTaggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShipRepository")
 * @ORM\Table(name="ship")
 * @ORM\HasLifecycleCallbacks()
 */
class Ship extends AbstractTaggable
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $regNumber = '';

    /**
     * @ORM\Column(type="smallint")
     */
    private $maxPeople = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShipType")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShipModel")
     */
    private $model;

    /**
     * @ORM\ManyToMany(targetEntity="ShipImage")
     * @ORM\JoinTable(name="ships_images",
     *      joinColumns={@ORM\JoinColumn(name="ship_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="ships_tags",
     *      joinColumns={@ORM\JoinColumn(name="ship_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    protected $tags;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Port")
     */
    private $port;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShipSchedule", mappedBy="ship", cascade={"persist"})
     */
    private $rentSchedule;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShipScheduleExclude", mappedBy="ship", orphanRemoval=true)
     */
    private $rentScheduleExclude;

    /**
     * @ORM\OneToMany(targetEntity="OrderRent", mappedBy="ship")
     */
    private $orderRents;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowRent = false;
    /**
     * @ORM\Column(type="boolean")
     */
    private $allowExcursion = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $baseRentPrice = 0;

    /**
     * Минимальное время аренды в минутах
     *
     * @ORM\Column(type="integer")
     */
    private $minRentTime = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxChild = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RentOption", mappedBy="ship", orphanRemoval=true, cascade={"persist"})
     */
    private $rentOptions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="ships")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $defaultLanguage = Lang::RU;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $defaultCurrency = Currency::RUB;

    /**
     * @ORM\Column(type="enum_duration")
     */
    private $defaultDuration = EnumDurationType::HOUR;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ship\Tarif", mappedBy="ship", orphanRemoval=true, cascade={"persist"})
     */
    private $tarifs;

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
        $this->rentSchedule = new ArrayCollection();
        $this->orderRents = new ArrayCollection();
        $this->rentScheduleExclude = new ArrayCollection();
        $this->rentOptions = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush(){
        $this->mergeNewTranslations();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->translate($this->defaultLanguage)->getName();
    }

    public function setName(string $name): self
    {
        $this->translate($this->defaultLanguage)->setName($name);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->translate($this->defaultLanguage)->getDescription();
    }

    public function setDescription(?string $description): self
    {
        $this->translate($this->defaultLanguage)->setDescription($description);

        return $this;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): self
    {
        $this->regNumber = $regNumber;

        return $this;
    }

    public function getMaxPeople(): ?int
    {
        return $this->maxPeople;
    }

    public function setMaxPeople(int $maxPeople): self
    {
        $this->maxPeople = $maxPeople;

        return $this;
    }

    public function getType(): ?ShipType
    {
        return $this->type;
    }

    public function setType(?ShipType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getModel(): ?ShipModel
    {
        return $this->model;
    }

    public function setModel(?ShipModel $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return Collection|ShipImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ShipImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(ShipImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    public function setTagsText(?string $tagsText): void
    {
        $this->updated = new \DateTimeImmutable();
        parent::setTagsText($tagsText);
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPort(): ?Port
    {
        return $this->port;
    }

    public function setPort(?Port $port): self
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return Collection|ShipSchedule[]
     */
    public function getRentSchedule(): Collection
    {
        return $this->rentSchedule;
    }

    public function addRentSchedule(ShipSchedule $rentSchedule): self
    {
        if (!$this->rentSchedule->contains($rentSchedule)) {
            $this->rentSchedule[] = $rentSchedule;
            $rentSchedule->setShip($this);
        }

        return $this;
    }

    public function removeRentSchedule(ShipSchedule $rentSchedule): self
    {
        if ($this->rentSchedule->contains($rentSchedule)) {
            $this->rentSchedule->removeElement($rentSchedule);
            // set the owning side to null (unless already changed)
            if ($rentSchedule->getShip() === $this) {
                $rentSchedule->setShip(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderRent[]
     */
    public function getOrderRents(): Collection
    {
        return $this->orderRents;
    }

    public function addOrderRent(OrderRent $orderRent): self
    {
        if (!$this->orderRents->contains($orderRent)) {
            $this->orderRents[] = $orderRent;
            $orderRent->setShip($this);
        }

        return $this;
    }

    public function removeOrderRent(OrderRent $orderRent): self
    {
        if ($this->orderRents->contains($orderRent)) {
            $this->orderRents->removeElement($orderRent);
            // set the owning side to null (unless already changed)
            if ($orderRent->getShip() === $this) {
                $orderRent->setShip(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ShipScheduleExclude[]
     */
    public function getRentScheduleExclude(): Collection
    {
        return $this->rentScheduleExclude;
    }

    public function addRentScheduleExclude(ShipScheduleExclude $rentScheduleExclude): self
    {
        if (!$this->rentScheduleExclude->contains($rentScheduleExclude)) {
            $this->rentScheduleExclude[] = $rentScheduleExclude;
            $rentScheduleExclude->setShip($this);
        }

        return $this;
    }

    public function removeRentScheduleExclude(ShipScheduleExclude $rentScheduleExclude): self
    {
        if ($this->rentScheduleExclude->contains($rentScheduleExclude)) {
            $this->rentScheduleExclude->removeElement($rentScheduleExclude);
            // set the owning side to null (unless already changed)
            if ($rentScheduleExclude->getShip() === $this) {
                $rentScheduleExclude->setShip(null);
            }
        }

        return $this;
    }

    public function getAllowRent(): ?bool
    {
        return $this->allowRent;
    }

    public function setAllowRent(bool $allowRent): self
    {
        $this->allowRent = $allowRent;

        return $this;
    }

    public function getBaseRentPrice(): ?int
    {
        return $this->baseRentPrice;
    }

    public function setBaseRentPrice(int $baseRentPrice): self
    {
        $this->baseRentPrice = $baseRentPrice;

        return $this;
    }

    public function getMinRentTime(): ?int
    {
        return $this->minRentTime;
    }

    public function setMinRentTime(int $minRentTime): self
    {
        $this->minRentTime = $minRentTime;

        return $this;
    }

    public function getMaxChild(): ?int
    {
        return $this->maxChild;
    }

    public function setMaxChild(int $maxChild): self
    {
        $this->maxChild = $maxChild;

        return $this;
    }

    /**
     * @return Collection|RentOption[]
     */
    public function getRentOptions(): Collection
    {
        return $this->rentOptions;
    }

    public function addRentOption(RentOption $rentOption): self
    {
        if (!$this->rentOptions->contains($rentOption)) {
            $this->rentOptions[] = $rentOption;
            $rentOption->setShip($this);
        }

        return $this;
    }

    public function removeRentOption(RentOption $rentOption): self
    {
        if ($this->rentOptions->contains($rentOption)) {
            $this->rentOptions->removeElement($rentOption);
            // set the owning side to null (unless already changed)
            if ($rentOption->getShip() === $this) {
                $rentOption->setShip(null);
            }
        }

        return $this;
    }

    public function getAllowExcursion(): ?bool
    {
        return $this->allowExcursion;
    }

    public function setAllowExcursion(bool $allowExcursion): self
    {
        $this->allowExcursion = $allowExcursion;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getDefaultLanguage(): ?string
    {
        return $this->defaultLanguage;
    }

    public function setDefaultLanguage(string $defaultLanguage): self
    {
        $this->defaultLanguage = $defaultLanguage;

        return $this;
    }

    public function getDefaultCurrency(): ?string
    {
        return $this->defaultCurrency;
    }

    public function setDefaultCurrency(string $defaultCurrency): self
    {
        $this->defaultCurrency = $defaultCurrency;

        return $this;
    }

    public function getDefaultDuration()
    {
        return $this->defaultDuration;
    }

    public function setDefaultDuration($defaultDuration): self
    {
        $this->defaultDuration = $defaultDuration;

        return $this;
    }

    /**
     * @return Collection|Tarif[]
     */
    public function getTarifs(): Collection
    {
        return $this->tarifs;
    }

    public function addTarif(Tarif $tarif): self
    {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs[] = $tarif;
            $tarif->setShip($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self
    {
        if ($this->tarifs->contains($tarif)) {
            $this->tarifs->removeElement($tarif);
            // set the owning side to null (unless already changed)
            if ($tarif->getShip() === $this) {
                $tarif->setShip(null);
            }
        }

        return $this;
    }
}
