<?php
namespace App\Wsdl\AstraMarin;
class GetTicketTypeResponse
{

    /**
     * @var ResultOperation $return
     */
    protected $return = null;

    /**
     * @param ResultOperation $return
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

    /**
     * @return ResultOperation
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param ResultOperation $return
     * @return GetTicketTypeResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
