<?php
namespace App\Wsdl\AstraMarin;
class GetAmountCertificate
{

    /**
     * @var string $StringJSON
     */
    protected $StringJSON = null;

    /**
     * @param string $StringJSON
     */
    public function __construct($StringJSON)
    {
      $this->StringJSON = $StringJSON;
    }

    /**
     * @return string
     */
    public function getStringJSON()
    {
      return $this->StringJSON;
    }

    /**
     * @param string $StringJSON
     * @return GetAmountCertificate
     */
    public function setStringJSON($StringJSON)
    {
      $this->StringJSON = $StringJSON;
      return $this;
    }

}
