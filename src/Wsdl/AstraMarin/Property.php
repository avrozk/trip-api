<?php
namespace App\Wsdl\AstraMarin;
class Property
{

    /**
     * @var ResultOperation $Menu
     */
    protected $Menu = null;

    /**
     * @param ResultOperation $Menu
     */
    public function __construct($Menu)
    {
      $this->Menu = $Menu;
    }

    /**
     * @return ResultOperation
     */
    public function getMenu()
    {
      return $this->Menu;
    }

    /**
     * @param ResultOperation $Menu
     * @return Property
     */
    public function setMenu($Menu)
    {
      $this->Menu = $Menu;
      return $this;
    }

}
