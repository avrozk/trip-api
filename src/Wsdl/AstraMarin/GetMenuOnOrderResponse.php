<?php
namespace App\Wsdl\AstraMarin;
class GetMenuOnOrderResponse
{

    /**
     * @var ResultOperation $return
     */
    protected $return = null;

    /**
     * @param ResultOperation $return
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

    /**
     * @return ResultOperation
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param ResultOperation $return
     * @return GetMenuOnOrderResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
