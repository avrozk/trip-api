<?php

namespace App\Wsdl\AstraMarin;

/**
 *
 */
class InternetSaleJSON extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'Property' => 'App\\Wsdl\\AstraMarin\\Property',
      'ResultOperation' => 'App\\Wsdl\\AstraMarin\\ResultOperation',
      'ServicesOnDate' => 'App\\Wsdl\\AstraMarin\\ServicesOnDate',
      'ServicesOnDateResponse' => 'App\\Wsdl\\AstraMarin\\ServicesOnDateResponse',
      'EventsOnDate' => 'App\\Wsdl\\AstraMarin\\EventsOnDate',
      'EventsOnDateResponse' => 'App\\Wsdl\\AstraMarin\\EventsOnDateResponse',
      'SeatsOnEvent' => 'App\\Wsdl\\AstraMarin\\SeatsOnEvent',
      'SeatsOnEventResponse' => 'App\\Wsdl\\AstraMarin\\SeatsOnEventResponse',
      'BookingSeat' => 'App\\Wsdl\\AstraMarin\\BookingSeat',
      'BookingSeatResponse' => 'App\\Wsdl\\AstraMarin\\BookingSeatResponse',
      'CancelBookingSeat' => 'App\\Wsdl\\AstraMarin\\CancelBookingSeat',
      'CancelBookingSeatResponse' => 'App\\Wsdl\\AstraMarin\\CancelBookingSeatResponse',
      'GetSeatPrice' => 'App\\Wsdl\\AstraMarin\\GetSeatPrice',
      'GetSeatPriceResponse' => 'App\\Wsdl\\AstraMarin\\GetSeatPriceResponse',
      'SeatСategory' => 'App\\Wsdl\\AstraMarin\\Seatategory',
      'SeatСategoryResponse' => 'App\\Wsdl\\AstraMarin\\SeatategoryResponse',
      'GetTicketType' => 'App\\Wsdl\\AstraMarin\\GetTicketType',
      'GetTicketTypeResponse' => 'App\\Wsdl\\AstraMarin\\GetTicketTypeResponse',
      'GetPaymentType' => 'App\\Wsdl\\AstraMarin\\GetPaymentType',
      'GetPaymentTypeResponse' => 'App\\Wsdl\\AstraMarin\\GetPaymentTypeResponse',
      'ChekDeposit' => 'App\\Wsdl\\AstraMarin\\ChekDeposit',
      'ChekDepositResponse' => 'App\\Wsdl\\AstraMarin\\ChekDepositResponse',
      'GetDeposit' => 'App\\Wsdl\\AstraMarin\\GetDeposit',
      'GetDepositResponse' => 'App\\Wsdl\\AstraMarin\\GetDepositResponse',
      'CheckPromoCode' => 'App\\Wsdl\\AstraMarin\\CheckPromoCode',
      'CheckPromoCodeResponse' => 'App\\Wsdl\\AstraMarin\\CheckPromoCodeResponse',
      'MenuOnTypePrice' => 'App\\Wsdl\\AstraMarin\\MenuOnTypePrice',
      'MenuOnTypePriceResponse' => 'App\\Wsdl\\AstraMarin\\MenuOnTypePriceResponse',
      'SaleSeat' => 'App\\Wsdl\\AstraMarin\\SaleSeat',
      'SaleSeatResponse' => 'App\\Wsdl\\AstraMarin\\SaleSeatResponse',
      'PaymentConfirmation' => 'App\\Wsdl\\AstraMarin\\PaymentConfirmation',
      'PaymentConfirmationResponse' => 'App\\Wsdl\\AstraMarin\\PaymentConfirmationResponse',
      'GetServiceGroup' => 'App\\Wsdl\\AstraMarin\\GetServiceGroup',
      'GetServiceGroupResponse' => 'App\\Wsdl\\AstraMarin\\GetServiceGroupResponse',
      'ReturnOrder' => 'App\\Wsdl\\AstraMarin\\ReturnOrder',
      'ReturnOrderResponse' => 'App\\Wsdl\\AstraMarin\\ReturnOrderResponse',
      'GetDiscountCard' => 'App\\Wsdl\\AstraMarin\\GetDiscountCard',
      'GetDiscountCardResponse' => 'App\\Wsdl\\AstraMarin\\GetDiscountCardResponse',
      'RegisterDiscountCard' => 'App\\Wsdl\\AstraMarin\\RegisterDiscountCard',
      'RegisterDiscountCardResponse' => 'App\\Wsdl\\AstraMarin\\RegisterDiscountCardResponse',
      'GetInformationSource' => 'App\\Wsdl\\AstraMarin\\GetInformationSource',
      'GetInformationSourceResponse' => 'App\\Wsdl\\AstraMarin\\GetInformationSourceResponse',
      'GetDish' => 'App\\Wsdl\\AstraMarin\\GetDish',
      'GetDishResponse' => 'App\\Wsdl\\AstraMarin\\GetDishResponse',
      'GetMenuOnOrder' => 'App\\Wsdl\\AstraMarin\\GetMenuOnOrder',
      'GetMenuOnOrderResponse' => 'App\\Wsdl\\AstraMarin\\GetMenuOnOrderResponse',
      'MenuAvailable' => 'App\\Wsdl\\AstraMarin\\MenuAvailable',
      'MenuAvailableResponse' => 'App\\Wsdl\\AstraMarin\\MenuAvailableResponse',
      'GetDishOnOrder' => 'App\\Wsdl\\AstraMarin\\GetDishOnOrder',
      'GetDishOnOrderResponse' => 'App\\Wsdl\\AstraMarin\\GetDishOnOrderResponse',
      'SaleDish' => 'App\\Wsdl\\AstraMarin\\SaleDish',
      'SaleDishResponse' => 'App\\Wsdl\\AstraMarin\\SaleDishResponse',
      'GetAmountCertificate' => 'App\\Wsdl\\AstraMarin\\GetAmountCertificate',
      'GetAmountCertificateResponse' => 'App\\Wsdl\\AstraMarin\\GetAmountCertificateResponse',
      'GetOrder' => 'App\\Wsdl\\AstraMarin\\GetOrder',
      'GetOrderResponse' => 'App\\Wsdl\\AstraMarin\\GetOrderResponse',
      'GetCustomerOrders' => 'App\\Wsdl\\AstraMarin\\GetCustomerOrders',
      'GetCustomerOrdersResponse' => 'App\\Wsdl\\AstraMarin\\GetCustomerOrdersResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'InternetSaleJSON.wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param ServicesOnDate $parameters
     * @return ServicesOnDateResponse
     */
    public function ServicesOnDate(ServicesOnDate $parameters)
    {
      return $this->__soapCall('ServicesOnDate', array($parameters));
    }

    /**
     * @param EventsOnDate $parameters
     * @return EventsOnDateResponse
     */
    public function EventsOnDate(EventsOnDate $parameters)
    {
      return $this->__soapCall('EventsOnDate', array($parameters));
    }

    /**
     * @param SeatsOnEvent $parameters
     * @return SeatsOnEventResponse
     */
    public function SeatsOnEvent(SeatsOnEvent $parameters)
    {
      return $this->__soapCall('SeatsOnEvent', array($parameters));
    }

    /**
     * @param BookingSeat $parameters
     * @return BookingSeatResponse
     */
    public function BookingSeat(BookingSeat $parameters)
    {
      return $this->__soapCall('BookingSeat', array($parameters));
    }

    /**
     * @param CancelBookingSeat $parameters
     * @return CancelBookingSeatResponse
     */
    public function CancelBookingSeat(CancelBookingSeat $parameters)
    {
      return $this->__soapCall('CancelBookingSeat', array($parameters));
    }

    /**
     * @param GetSeatPrice $parameters
     * @return GetSeatPriceResponse
     */
    public function GetSeatPrice(GetSeatPrice $parameters)
    {
      return $this->__soapCall('GetSeatPrice', array($parameters));
    }

    /**
     * @param Seatategory $parameters
     * @return SeatСategoryResponse
     */
    public function Seatategory($parameters)
    {
      return $this->__soapCall('SeatСategory', array($parameters));
    }

    /**
     * @param GetTicketType $parameters
     * @return GetTicketTypeResponse
     */
    public function GetTicketType(GetTicketType $parameters)
    {
      return $this->__soapCall('GetTicketType', array($parameters));
    }

    /**
     * @param GetPaymentType $parameters
     * @return GetPaymentTypeResponse
     */
    public function GetPaymentType(GetPaymentType $parameters)
    {
      return $this->__soapCall('GetPaymentType', array($parameters));
    }

    /**
     * @param ChekDeposit $parameters
     * @return ChekDepositResponse
     */
    public function ChekDeposit(ChekDeposit $parameters)
    {
      return $this->__soapCall('ChekDeposit', array($parameters));
    }

    /**
     * @param GetDeposit $parameters
     * @return GetDepositResponse
     */
    public function GetDeposit(GetDeposit $parameters)
    {
      return $this->__soapCall('GetDeposit', array($parameters));
    }

    /**
     * @param CheckPromoCode $parameters
     * @return CheckPromoCodeResponse
     */
    public function CheckPromoCode(CheckPromoCode $parameters)
    {
      return $this->__soapCall('CheckPromoCode', array($parameters));
    }

    /**
     * @param MenuOnTypePrice $parameters
     * @return MenuOnTypePriceResponse
     */
    public function MenuOnTypePrice(MenuOnTypePrice $parameters)
    {
      return $this->__soapCall('MenuOnTypePrice', array($parameters));
    }

    /**
     * @param SaleSeat $parameters
     * @return SaleSeatResponse
     */
    public function SaleSeat(SaleSeat $parameters)
    {
      return $this->__soapCall('SaleSeat', array($parameters));
    }

    /**
     * @param PaymentConfirmation $parameters
     * @return PaymentConfirmationResponse
     */
    public function PaymentConfirmation(PaymentConfirmation $parameters)
    {
      return $this->__soapCall('PaymentConfirmation', array($parameters));
    }

    /**
     * @param GetServiceGroup $parameters
     * @return GetServiceGroupResponse
     */
    public function GetServiceGroup(GetServiceGroup $parameters)
    {
      return $this->__soapCall('GetServiceGroup', array($parameters));
    }

    /**
     * @param ReturnOrder $parameters
     * @return ReturnOrderResponse
     */
    public function ReturnOrder(ReturnOrder $parameters)
    {
      return $this->__soapCall('ReturnOrder', array($parameters));
    }

    /**
     * @param GetDiscountCard $parameters
     * @return GetDiscountCardResponse
     */
    public function GetDiscountCard(GetDiscountCard $parameters)
    {
      return $this->__soapCall('GetDiscountCard', array($parameters));
    }

    /**
     * @param RegisterDiscountCard $parameters
     * @return RegisterDiscountCardResponse
     */
    public function RegisterDiscountCard(RegisterDiscountCard $parameters)
    {
      return $this->__soapCall('RegisterDiscountCard', array($parameters));
    }

    /**
     * @param GetInformationSource $parameters
     * @return GetInformationSourceResponse
     */
    public function GetInformationSource(GetInformationSource $parameters)
    {
      return $this->__soapCall('GetInformationSource', array($parameters));
    }

    /**
     * @param GetDish $parameters
     * @return GetDishResponse
     */
    public function GetDish(GetDish $parameters)
    {
      return $this->__soapCall('GetDish', array($parameters));
    }

    /**
     * @param GetMenuOnOrder $parameters
     * @return GetMenuOnOrderResponse
     */
    public function GetMenuOnOrder(GetMenuOnOrder $parameters)
    {
      return $this->__soapCall('GetMenuOnOrder', array($parameters));
    }

    /**
     * @param MenuAvailable $parameters
     * @return MenuAvailableResponse
     */
    public function MenuAvailable(MenuAvailable $parameters)
    {
      return $this->__soapCall('MenuAvailable', array($parameters));
    }

    /**
     * @param GetDishOnOrder $parameters
     * @return GetDishOnOrderResponse
     */
    public function GetDishOnOrder(GetDishOnOrder $parameters)
    {
      return $this->__soapCall('GetDishOnOrder', array($parameters));
    }

    /**
     * @param SaleDish $parameters
     * @return SaleDishResponse
     */
    public function SaleDish(SaleDish $parameters)
    {
      return $this->__soapCall('SaleDish', array($parameters));
    }

    /**
     * @param GetAmountCertificate $parameters
     * @return GetAmountCertificateResponse
     */
    public function GetAmountCertificate(GetAmountCertificate $parameters)
    {
      return $this->__soapCall('GetAmountCertificate', array($parameters));
    }

    /**
     * @param GetOrder $parameters
     * @return GetOrderResponse
     */
    public function GetOrder(GetOrder $parameters)
    {
      return $this->__soapCall('GetOrder', array($parameters));
    }

    /**
     * @param GetCustomerOrders $parameters
     * @return GetCustomerOrdersResponse
     */
    public function GetCustomerOrders(GetCustomerOrders $parameters)
    {
      return $this->__soapCall('GetCustomerOrders', array($parameters));
    }

}
